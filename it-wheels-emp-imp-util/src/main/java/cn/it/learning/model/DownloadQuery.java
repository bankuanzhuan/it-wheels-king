package cn.it.learning.model;

import cn.it.learning.base.enums.ExcelTypeEnums;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**下载统一封装
 * @author wyh
 * @date 2022/11/20 /21:26
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DownloadQuery<T> implements Serializable {
    private static final long serialVersionUID=2103755840788370333L;
    /**下载文件类型*/
    private ExcelTypeEnums type;
    /**下载文件名称*/
    private String fileName;
    /**查询条件*/
    private T query;
}
