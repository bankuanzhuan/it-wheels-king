package cn.it.learning.model;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * 导出抽象实体
 *
 * @date 2022/11/20 /22:10
 */
@Data
public abstract class DynamicFieldExcelEntity<T> implements Serializable {
    private static final long serialVersionUID = 819269039455510360L;
    @JSONField(serialize = false)
    private List<Node> list;

    public void setFieldList(List<T> enities) {
        list = this.transitionNode(enities);
    }

    //抽象方法，由子类去实现
    protected abstract List<Node> transitionNode(List<T> entity);

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Node {
        private String key;
        private Object value;

    }
    //这里把集合的key和value给分别添加到请求头和data集合中
    public void initDynamicField(List<Object> data, Collection<String> heads) {
        if (CollUtil.isNotEmpty(list)) {
            for (Node node : list) {
                heads.add(node.getKey());
                data.add(node.getValue());
            }
        }
    }
}
