package cn.it.learning.model;


import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @date 2022/11/20 /22:08
 * @Description： 导出excel实体
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserToExcelEntity extends DynamicFieldExcelEntity<UserFieldDetailVo> {
    @JSONField(name = "用户Id", ordinal = 1)
    private Integer Id;
    @JSONField(name = "用户名称", ordinal = 2)
    private String name;
    @JSONField(name = "用户密码", ordinal = 3)
    private String password;

    @Override
    protected List<Node> transitionNode(List<UserFieldDetailVo> entity) {
        return entity.stream().map(o -> {
            //这里如果想输出3个值，比如说集合3个属性，那也可以再从node中增加一个属性来扩展
            Node node = new Node();
            node.setKey(o.getStartDate());
            node.setValue(o.getAge());
            return node;
        }).collect(Collectors.toList());
    }
}
