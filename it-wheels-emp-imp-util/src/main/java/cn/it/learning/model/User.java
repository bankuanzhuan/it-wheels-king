package cn.it.learning.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @Author it-learning-diary
 * @Description 测试实体
 * @Date 2022/4/25 9:59
 * @Version 1.0
 */
@Getter
@Setter
@TableName(value = "t_user_test")
@AllArgsConstructor
@NoArgsConstructor
public class User {

    private Integer id;
    private String name;
    private String password;
    /**增加一个集合，我们要把集合来导出*/
    private List<UserFieldDetailVo> userFieldDetailVoList;


}
