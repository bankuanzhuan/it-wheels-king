package cn.it.learning.util.bean;

import org.springframework.beans.BeanUtils;

import java.util.Objects;
import java.util.function.Supplier;

/**

 * @date 2022/11/20 /21:16
 */

public class BoostBeanUtils {
    /**快速拷贝增强方法*/
    public static <T,R>R copyPropertiesTies(T t, Supplier<R> supplier){
        if(Objects.isNull(t)){
            return null;
        }
        R r=supplier.get();
        BeanUtils.copyProperties(t,r);
        return r;
    }
}
