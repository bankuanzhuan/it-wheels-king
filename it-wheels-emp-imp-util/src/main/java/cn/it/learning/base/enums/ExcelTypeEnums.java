package cn.it.learning.base.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@Getter
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ExcelTypeEnums {
    CSV("csv", "CSV"),
    XLSX("excel", "Excel");
    @EnumValue
    private final String code;
    private final String description;
    private static final Map<String, ExcelTypeEnums> LOOKUP = new HashMap<>();

    static {
        for (ExcelTypeEnums value : ExcelTypeEnums.values()) {
            LOOKUP.put(value.getCode(), value);
        }
    }

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static ExcelTypeEnums lookup(String code) {
        return LOOKUP.get(code);
    }


    public static ExcelTypeEnums ofCode(String code) {
        return lookup(code);
    }


}
